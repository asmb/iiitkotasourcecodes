package in.forsk.adapter;

import in.forsk.listviewwithcustombaseadapter_viewholder.R;
import in.forsk.wrapper.FacultyWrapper;

import java.util.ArrayList;

import com.androidquery.AQuery;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//ArrayAdapter - A concrete BaseAdapter that is backed by an array of arbitrary objects(wrappers mostly).
public class FacultyListAdapter extends BaseAdapter {

	private final static String TAG = FacultyListAdapter.class.getSimpleName();

	// context to init the layout inflater service , which inflate the custom
	// Views(Resource layout)
	Context context;

	// layout resource id for the row layout(single row view)
	int resource;

	// to hold the data model (Instance variable )
	// as best practice ,data which is coming from an outside scope by
	// method/constructors
	// need to be hold for the lifetime of the class object
	// Similar we have created variable for holding context and resource id
	ArrayList<FacultyWrapper> mFacultyDataList;

	// Instantiates a layout XML file into its corresponding View objects.
	// In other words, it takes as input an XML file and builds the View objects
	// from it.
	LayoutInflater inflater;

	// 3rd party library to do our dirty work(background threading)
	AQuery aq;

	public FacultyListAdapter(Context context, int resource, ArrayList<FacultyWrapper> objects) {

		this.context = context;
		this.resource = resource;
		this.mFacultyDataList = (ArrayList<FacultyWrapper>) objects;

		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		aq = new AQuery(context);
	}

	// This is the place where you design your row view
	// this is called multiple times, depending on the data in the data model
	// this is also called , when the view needs to be recycled
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHoder holder;
		if (convertView == null) {
			convertView = inflater.inflate(resource, null);
			Log.d(TAG, "New View init - " + position);
			
			// well set up the ViewHolder
			holder	=	new ViewHoder(convertView);
			
			// store the holder with the view.
			convertView.setTag(holder);
			
		} else {
			Log.d(TAG, "Recycling old Views - " + position);
			
			// we've just avoided calling findViewById() on resource everytime
	        // just use the viewHolder
			holder	=	(ViewHoder) convertView.getTag();
			
		}

		// Retrieve the data at particular index(position)
		// so we can bind the correct data
		// FacultyWrapper obj = mFacultyDataList.get(position);
		FacultyWrapper obj = (FacultyWrapper) getItem(position);

		/**Now We dont need to do this all
		 * 
		// Every time get view is called , it has to internally traverse view
		// hierarchy (findViewById)
		// to find the correct view id and create a reference to object
		// if the row layout complexity increases, it takes hell lot of time
		TextView nameTv = (TextView) convertView.findViewById(R.id.nameTv);
		TextView departmentTv = (TextView) convertView.findViewById(R.id.departmentTv);
		TextView reserch_areaTv = (TextView) convertView.findViewById(R.id.reserch_areaTv);

		ImageView profileIv = (ImageView) convertView.findViewById(R.id.profileIv);
		 * 
		 */
		

		//Now we are accessing same object via holder 
		//As now we are not using findviewbyId so we don't need to traverse view hierarchy
		//this will increase the list view performance
		AQuery temp_aq = aq.recycle(convertView);
		temp_aq.id(holder.profileIv).image(obj.getPhoto(), true, true, 200, 0);

		holder.nameTv.setText(obj.getFirst_name() + " " + obj.getLast_name());
		holder.departmentTv.setText(obj.getDepartment());
		holder.reserch_areaTv.setText(obj.getReserch_area());

		Log.d(TAG, "get View - " + position);

		return convertView;
	}

	//The ViewHolder design pattern enables you to access each list item view without the need for the look up, 
	//saving valuable processor cycles. Specifically, it avoids frequent call of findViewById() during ListView 
	//scrolling, and that will make it smooth.
	//http://www.javacodegeeks.com/2013/09/android-viewholder-pattern-example.html
	public static class ViewHoder {
		ImageView profileIv;
		TextView nameTv, departmentTv, reserch_areaTv;

		public ViewHoder(View view) {
			profileIv = (ImageView) view.findViewById(R.id.profileIv);

			nameTv = (TextView) view.findViewById(R.id.nameTv);
			departmentTv = (TextView) view.findViewById(R.id.departmentTv);
			reserch_areaTv = (TextView) view.findViewById(R.id.reserch_areaTv);
		}
	}

	// These are the 3 new methods we need to override in base adapter
	@Override
	public int getCount() {
		// This is called internally by the get view method
		// this is the max number on rows adapter created
		return mFacultyDataList.size();
	}

	@Override
	public Object getItem(int position) {
		// this return object at particular position
		// we can use this method to apply custom functionality or filtration
		return mFacultyDataList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// this method is use to maintain uniqueness of each row
		return position;
	}

}
